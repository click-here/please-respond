import collections
from meetup_calc.records import EventRecord


class ScoringAggregator:

    def __init__(self, rsvp_bucket, event_bucket):
        self.rsvp_bucket = rsvp_bucket
        self.event_bucket = event_bucket
        self.counted_rsvps = []

    def _get_furthest_event(self):
        """Gets the furthest event in the bucket."""
        greatest_event = EventRecord(mtime='1',
                                     record_id='1',
                                     event_time='1',
                                     event_name='name',
                                     event_url='url'
                                     )

        for event in self.event_bucket:
            if event.event_time >= greatest_event.event_time:
                greatest_event = event

        return greatest_event

    def _get_furthest_event_date_and_url(self):
        """Gets furthest event date and url"""
        furthest_event = self._get_furthest_event()
        event_url = furthest_event.event_url
        furthest_event_as_ms = furthest_event.ms_since_epoch_to_seconds(furthest_event.event_time)
        event_date = furthest_event.get_human_date(furthest_event_as_ms)
        return event_date, event_url

    def _count_rsvps(self):
        """
        This is for counting RSVPs.

        "Yes" is 1 RSVP
        "No" is 1 RSVP

        "Yes" with n guests is n+1 RSVPs.
        "No" with guests is n+1 RSVPs (0.04% incidence)

        "Waitlist" and other response types are treated the same.
        """
        rsvp_count = 0

        for rsvp in self.rsvp_bucket:
            rsvp_count = rsvp_count + rsvp.guests + 1
            self.counted_rsvps.append(rsvp)

        return rsvp_count

    def _group_by_countries(self):
        """Creates a dictionary which contains a count of RSVP counts by country."""
        self.country_count_dict = {}
        for counted_rsvp in self.counted_rsvps:
            group_country = counted_rsvp.group_country
            if group_country in self.country_count_dict:
                self.country_count_dict[group_country] += counted_rsvp.guests + 1
            else:
                self.country_count_dict[group_country] = counted_rsvp.guests + 1

    def _top_3_countries(self):
        """
        Returns top 3 countries as collections object.
        Elements with equal counts are ordered arbitrarily
        """
        self._group_by_countries()
        return collections.Counter(self.country_count_dict).most_common(3)

    def calculate(self):
        """The method which should be used for getting the final calculation string"""
        rsvp_count = str(self._count_rsvps())
        event_date, event_url = self._get_furthest_event_date_and_url()
        top_3_countries = self._top_3_countries()
        top_3_countries_string = ",".join(f"{x[0]},{str(x[1])}" for x in top_3_countries)
        return ','.join([rsvp_count, event_date, event_url, top_3_countries_string])

