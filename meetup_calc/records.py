from datetime import datetime


def typename(obj):
    return type(obj).__name__


class Record:

    def __init__(self, mtime=None, record_id=None):
        """
        Everything comes in as a string, and normally I would have dedicated setter/getter for type casting
        but in the spirit of not over-engineering, I've kept this simple.
        """
        self.mtime = int(mtime)
        self.record_id = record_id

    @staticmethod
    def get_human_date(seconds_since_epoch):
        return datetime.utcfromtimestamp(seconds_since_epoch).strftime('%Y-%m-%d %H:%M')

    @staticmethod
    def ms_since_epoch_to_seconds(milliseconds_since_epoch):
        """Meetup gives time in milliseconds since epoch. This converts that to seconds since epoch"""
        return milliseconds_since_epoch / 1000

    def __repr__(self):
        """For showing a developer friendly representation of the object."""
        return f"{typename(self)} (id={self.record_id})"


class RSVPRecord(Record):
    """
    A class for holding rsvp record data.
    The record_id should be set from the `rsvp_id` from the streaming data so that
    the RecordBucket can keep unique RSVP records
    """

    def __init__(self,
                 mtime=None,
                 record_id=None,
                 response=None,
                 member_id=None,
                 guests=None,
                 group_country=None,
                 group_id=None):
        super().__init__(mtime, record_id)
        self.response = response
        self.member_id = int(member_id)
        self.guests = int(guests)
        self.group_country = group_country
        self.group_id = group_id

    def __str__(self):
        return f"RSVP of '{self.response}' with rsvp_id of '{str(self.record_id)}' and guest count of '{str(self.guests)}'"


class EventRecord(Record):
    """
    A class for holding event record data.
    The record_id should be set from the `event_id` from the streaming data so that
    the RecordBucket can keep unique RSVP records
    """
    def __init__(self,
                 mtime=None,
                 record_id=None,
                 event_time=None,
                 event_name=None,
                 event_url=None):
        super().__init__(mtime, record_id)
        self.event_time = int(event_time)
        self.event_name = event_name
        self.event_url = event_url

    def __str__(self):
        event_date_formatted = self.get_human_date(self.event_time)
        return f"Event named '{self.event_name}' at {event_date_formatted} UTC"



