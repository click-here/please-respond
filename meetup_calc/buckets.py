from meetup_calc.records import Record


class RecordBucket:
    """
    A base class with shared methods for holding stream records.
    Duplicate records are not permitted in a bucket. Duplicates are defined by record_id.
    So when using the subclasses EventBucket and RSVPBucket the record_id should be set to the
    id that makes records unique for that bucket type.

    For RSVPs this is 'rsvp_id'
    For Event this is 'event_id'
    """

    def __init__(self):
        self._records = dict()

    @property
    def records(self):
        return self._records

    def _record_in_bucket(self, record):
        return record.record_id in self.records

    def _update_or_append(self, current_record):
        """
        Newly streamed records are checked for existence in the bucket. If not, they are added to the records dict.
        If the current_record exists and the mtime on it is greater than the one in the bucket then it replaces the one
        in the bucket.
        :param Record current_record:
        """
        if self._record_in_bucket(current_record):
            existing_record = self.records[current_record.record_id]
            if current_record.mtime > existing_record.mtime:
                self.records[current_record.record_id] = current_record
        else:
            self.records[current_record.record_id] = current_record

    def add(self, record):
        """
        Add method which references private method to allow easier use
        :param Record record:
        """
        self._update_or_append(record)

    def get(self, record_id):
        """
        Returns a record found by supplied record_id else raises key_error.
        Currently this is only used for the test suite.
        """
        if record_id in self.records:
            return self.records[record_id]
        else:
            raise KeyError(f"The record_id {str(record_id)} does not exist.")

    def __len__(self):
        """Returns count of records in the bucket."""
        return len(self.records)

    def __iter__(self):
        """Allows iteration over the Records in the `bucket` as Record objects"""
        for record_id, record in self.records.items():
            yield record


class EventBucket(RecordBucket):
    pass


class RSVPBucket(RecordBucket):
    pass

