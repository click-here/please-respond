import sys
import os

# path modification like this is generally frowned upon though I've chosen to do so here
# so that the python module didn't need to be installed nor have a user modifying their systems PYTHONPATH directly.
sys.path.append(os.getcwd())
import json
import time
import logging
import requests
from buckets import EventBucket, RSVPBucket
from records import EventRecord, RSVPRecord
from scoring import ScoringAggregator
import settings


if settings.LOGGING_ENABLED:
    logging.basicConfig(format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        filename='./output.log',
                        level=logging.DEBUG,
                        filemode='w')


class StreamCapture:

    def __init__(self, run_time_seconds):
        self.run_time_seconds = run_time_seconds
        self.logging_enabled = settings.LOGGING_ENABLED
        self.rsvp_bucket = RSVPBucket()
        self.event_bucket = EventBucket()
        self.stream_data = []

    def _calculate_scores(self):
        score_aggregator = ScoringAggregator(self.rsvp_bucket, self.event_bucket)
        score_string = score_aggregator.calculate()
        return score_string

    def run(self):
        start = time.time()
        req = requests.get('https://stream.meetup.com/2/rsvps', stream=True)
        for line in req.iter_lines():
            if line and time.time() - start < self.run_time_seconds:
                decoded_line = line.decode('utf-8')
                if settings.LOGGING_ENABLED:
                    print(time.time() - start)
                    logging.debug(decoded_line)

                self.stream_data.append(decoded_line)

                rec_json = json.loads(decoded_line)
                rsvp_record = RSVPRecord(mtime=rec_json['mtime'],
                                         record_id=rec_json['rsvp_id'],
                                         response=rec_json['response'],
                                         member_id=rec_json['member']['member_id'],
                                         guests=rec_json['guests'],
                                         group_country=rec_json['group']['group_country'],
                                         group_id=rec_json['group']['group_id'])

                event_record = EventRecord(mtime=rec_json['mtime'],
                                           record_id=rec_json['event']['event_id'],
                                           event_time=rec_json['event']['time'],
                                           event_name=rec_json['event']['event_name'],
                                           event_url=rec_json['event']['event_url'])

                self.rsvp_bucket.add(rsvp_record)
                self.event_bucket.add(event_record)

            else:
                break

        final_score_string = self._calculate_scores()
        return final_score_string

    def write_stream_to_file(self):
        """settings.SAVE_STREAM_DATA must be True for this to run.  Used primarily for debugging/manual validation."""
        with open("./output.jl", "w") as my_file:
            stream_string = '\n'.join(self.stream_data)
            my_file.write(stream_string)


if __name__ == '__main__':
    sc = StreamCapture(settings.RUN_TIME_SECONDS)
    output_string = sc.run()
    print(output_string)
    if settings.SAVE_STREAM_DATA:
        sc.write_stream_to_file()
