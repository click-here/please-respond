import unittest
import pathlib
import json

from meetup_calc.records import RSVPRecord, EventRecord
from meetup_calc.buckets import RSVPBucket, EventBucket
from meetup_calc.scoring import ScoringAggregator


def load_file(file_name):
    """Loads a test file a list, each line is stream record"""
    duplicate_test_data = pathlib.Path(__file__).parent.joinpath(file_name)
    with open(duplicate_test_data, 'r') as f:
        return f.readlines()


def build_rsvp_bucket(stream_records):
    rb = RSVPBucket()
    for rec in stream_records:
        rec_json = json.loads(rec)
        parsed_rec = RSVPRecord(mtime=rec_json['mtime'],
                                record_id=rec_json['rsvp_id'],
                                response=rec_json['response'],
                                member_id=rec_json['member']['member_id'],
                                guests=rec_json['guests'],
                                group_country=rec_json['group']['group_country'],
                                group_id=rec_json['group']['group_id']
                                )
        rb.add(parsed_rec)
    return rb


def build_event_bucket(stream_records):
    eb = EventBucket()
    for rec in stream_records:
        rec_json = json.loads(rec)
        parsed_rec = EventRecord(mtime=rec_json['mtime'],
                                 record_id=rec_json['event']['event_id'],
                                 event_time=rec_json['event']['time'],
                                 event_name=rec_json['event']['event_name'],
                                 event_url=rec_json['event']['event_url']
                                 )

        eb.add(parsed_rec)
    return eb


class TestRecords(unittest.TestCase):

    def test_duplicate_rsvp_ids(self):
        """This test validates that we're 'ignoring' duplicate stream records."""
        stream_records = load_file('duplicates.jsonl')
        rb = build_rsvp_bucket(stream_records)
        self.assertEqual(len(rb), 1)

    def test_country_change(self):
        """Checks if group host changed the groups country during the streaming process."""
        stream_records = load_file('country_change.jsonl')
        rb = build_rsvp_bucket(stream_records)
        oldest_record = rb.get(1858249711)
        self.assertEqual(oldest_record.group_country, "jp")

    def test_rsvp_response_change(self):
        """Someone may initially respond Yes and then later change to No"""
        stream_records = load_file('response_change.jsonl')
        rb = build_rsvp_bucket(stream_records)
        oldest_record = rb.get(1858249711)
        self.assertEqual(oldest_record.response, "no")

    def test_guest_count_change(self):
        """Someone may change the number of guests they are bringing"""
        stream_records = load_file('guest_count_changed.jsonl')
        rb = build_rsvp_bucket(stream_records)
        oldest_record = rb.get(1858249711)
        self.assertEqual(oldest_record.guests, 5)

    def test_misordered_data(self):
        """
        Some times data streamed later will actually be older than an earlier rsvp. We need to make sure that
        we only take the last record streamed if it's the oldest for that record_id
        """
        oldest_records_mtime = 1607530060000
        stream_records = load_file('misordered_data.jsonl')
        rb = build_rsvp_bucket(stream_records)
        oldest_record = rb.get(1858249711)
        self.assertEqual(oldest_record.mtime, oldest_records_mtime)


class TestEvents(unittest.TestCase):

    def test_event_url_change(self):
        """
        Tests if the url for the event has changed.
        Can't imagine why anyone would want to do this while collecting RSVPs but it could happen.
        """
        changed_url = r"https://www.meetup.com/changed_url/events/274975451/"
        stream_records = load_file('event_url_change.jsonl')
        eb = build_event_bucket(stream_records)
        oldest_record = eb.get('gpkfzrybcqbmb')
        self.assertEqual(oldest_record.event_url, changed_url)

    def test_event_time_change(self):
        """
        Tests for the date of event changing.
        Happens more frequently than one might think.
        """
        new_event_time = 1607557600000
        stream_records = load_file('event_time_changed.jsonl')
        eb = build_event_bucket(stream_records)
        oldest_record = eb.get('gpkfzrybcqbmb')
        self.assertEqual(oldest_record.event_time, new_event_time)

    def test_count_of_unique_events(self):
        stream_records = load_file('count_51_events.jsonl')
        eb = build_event_bucket(stream_records)
        self.assertEqual(len(eb), 51)


class TestScoring(unittest.TestCase):

    def test_furthest_event_and_date(self):
        stream_records = load_file('furthest_date_url.jsonl')
        eb = build_event_bucket(stream_records)
        rb = build_rsvp_bucket(stream_records)
        sa = ScoringAggregator(rb, eb)
        test_event_date, test_event_url = sa._get_furthest_event_date_and_url()
        correct_values = ('2021-01-30 10:30', 'https://www.meetup.com/worcester-socials-walks-hikes-wswh/events/275151906/')
        self.assertEqual((test_event_date, test_event_url), correct_values)

    def test_top_3(self):
        stream_records = load_file('top_3_countries.jl')
        eb = build_event_bucket(stream_records)
        rb = build_rsvp_bucket(stream_records)
        sa = ScoringAggregator(rb, eb)
        sa.calculate()
        top_3_countries = sa._top_3_countries()
        top_3_countries_string = ",".join(f"{x[0]},{str(x[1])}" for x in top_3_countries)
        self.assertEqual(top_3_countries_string, 'us,73,ca,8,au,5')


if __name__ == "__main__":
    unittest.main()
