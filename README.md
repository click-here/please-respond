# Description
This application connects to the Meetup rsvps streaming api end point (https://stream.meetup.com/2/rsvps) to collect events and rsvps.  
The following information is calculated and printed to the screen:
* Total # of RSVPs received<sup>1</sup>
* Date of Event furthest into the future
* URL for the Event furthest into the future
* The top 3 number of RSVPs received per Event host-country<sup>2</sup>

<sup>1</sup> Any response type is counted as an RSVP. Guests are counted as RSVPs.   
<sup>2</sup> If less than 3 countries are streamed it will return either top 2 or 1.

# Features
Since we are not connecting to any other API endpoint to get the most up to date event information we must ensure that if a detail about an event changes we capture it.

Edge cases that can or may occur that have been handled are as follows:

1. Duplicate "yes" or "no" RSVPs for the same person and same event.
2. Event URL change (by group owner changeing group name)
3. Event time changes
4. Country changes
5. A member changes guest count
6. Newer RSVPs sometimes have older mtimes than previously streamed rsvp's

# Installation
Requires python 3.6+
```bash
$ git clone https://gitlab.com/click-here/please-respond.git
$ cd please-respond/
$ python3 -m venv venv
$ source venv/bin/activate
$ (venv) pip install -r requirements.txt 
$ (venv) python3 meetup_calc/main.py
```


# Tests

```bash
$ (venv) python3 -m unittest
```

# Configuration
The `settings.py` file contains the ability to modify how long the program should run for via `RUN_TIME_SECONDS`.  Note, if `RUN_TIME_SECONDS=n` a payload at `n-1` will be saved. The first payload after `n` will be obtained and then dropped. So data is only captured for `n` seconds but the stream might be open for `n+10`.  More precise solutions are [certainly possible](https://github.com/psf/requests/issues/2806#issuecomment-146110398) but seemed overkill for this project.

Logging is supported and disabled by default.


# Notes
* Output timezone is UTC
* Regarding the top 3. ["Elements with equal counts are ordered arbitrarily:"](https://docs.python.org/3.6/library/collections.html#collections.Counter.most_common) 
* The response for the RSVP can be 'yes_pending_payment' or 'waitlist' in addition to 'yes' or 'no'.  These do not get counted any differently then a yes or no.
* During capture a single RSVP might come in for an event. After this, the group host changes the url or end date. If no more RSVPs come in then we would never know. 
